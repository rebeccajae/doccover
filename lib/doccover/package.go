package doccover

import (
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"path/filepath"
)

func ImportPackages(sourceDirectory string) (map[string]*ast.Package, error) {
	set := token.NewFileSet()
	packs := make(map[string]*ast.Package)
	subdirs, err := getDirsFromRoot(sourceDirectory)
	if err != nil {
		return nil, err
	}
	for _, dir := range subdirs {
		subpacks, err := parser.ParseDir(set, dir, nil, parser.ParseComments)
		if err != nil {
			return nil, err
		}
		for k, v := range subpacks {
			interpKey := filepath.Join(dir, k)
			packs[interpKey] = v
		}
	}
	return packs, nil
}

func getDirsFromRoot(root string) ([]string, error) {
	var dirs []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			dirs = append(dirs, path)
		}
		return nil
	})
	return dirs, err
}
