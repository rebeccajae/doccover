package doccover

import (
	"go/ast"
	"go/token"
	"unicode"
)

func ParsePackagesToTypes(pkgs map[string]*ast.Package) []*ast.TypeSpec {
	types := []*ast.TypeSpec{}
	for _, pkg := range pkgs {
		for _, f := range pkg.Files {
			for _, d := range f.Decls {
				if typeAsGenDef, isGenDecl := d.(*ast.GenDecl); isGenDecl && typeAsGenDef.Tok == token.TYPE {
					spec := typeAsGenDef.Specs[0].(*ast.TypeSpec)
					spec.Doc = typeAsGenDef.Doc
					types = append(types, spec)
				}
			}
		}
	}
	return types
}

func UnzipTypeDecls(types []*ast.TypeSpec) ([]*ast.TypeSpec, []*ast.TypeSpec) {
	private := []*ast.TypeSpec{}
	public := []*ast.TypeSpec{}

	for _, decl := range types {
		fc := []rune(decl.Name.Name)[0]
		if unicode.IsUpper(fc) {
			public = append(public, decl)
			continue
		}
		private = append(private, decl)
	}
	return public, private
}

func CountTypesWithDoc(types []*ast.TypeSpec) int {
	sum := 0
	for _, decl := range types {
		if len(decl.Doc.Text()) > 0 {
			sum += 1
		}
	}
	return sum
}
