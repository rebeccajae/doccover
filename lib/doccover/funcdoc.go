package doccover

import (
	"go/ast"
	"unicode"
)

func ParsePackagesToFuncs(pkgs map[string]*ast.Package) []*ast.FuncDecl {
	funcs := []*ast.FuncDecl{}
	for _, pkg := range pkgs {
		for _, f := range pkg.Files {
			for _, d := range f.Decls {
				if fn, isFn := d.(*ast.FuncDecl); isFn {
					funcs = append(funcs, fn)
				}
			}
		}
	}
	return funcs
}

func UnzipFuncDecls(types []*ast.FuncDecl) ([]*ast.FuncDecl, []*ast.FuncDecl) {
	private := []*ast.FuncDecl{}
	public := []*ast.FuncDecl{}

	for _, decl := range types {
		fc := []rune(decl.Name.Name)[0]
		if unicode.IsUpper(fc) {
			public = append(public, decl)
			continue
		}
		private = append(private, decl)
	}
	return public, private
}

func CountFuncsWithDoc(types []*ast.FuncDecl) int {
	sum := 0
	for _, decl := range types {
		if len(decl.Doc.Text()) > 0 {
			sum += 1
		}
	}
	return sum
}
