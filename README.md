# DocCover - A Go docstring coverage tool.
Everyone loves writing documentation comments, right? They're so easy to write.

No, really, they are. Please write doc comments.

Many teams have a concept of test coverage, where a regression should come with
an explanation. Coverage decreases, you should either have a good reason, or
add them.

But documentation comments are also important. Having an omission of
documentation could lead to confusion.

That's what DocCover is for.

DocCover will generate coverage reports of codebases written in Go.
