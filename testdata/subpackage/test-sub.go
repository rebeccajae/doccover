package subpackage

// PublicStruct is documented!
type PublicStruct struct {
	foo string
}

// privateStruct is also documented!
type privateStruct struct {
	bar string
}
