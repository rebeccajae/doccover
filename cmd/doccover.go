package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/rebeccajae/doclint/lib/doccover"
)

type Report struct {
	PublicFuncCovered  int
	PublicFuncTotal    int
	PrivateFuncCovered int
	PrivateFuncTotal   int
	PublicTypeCovered  int
	PublicTypeTotal    int
	PrivateTypeCovered int
	PrivateTypeTotal   int
}

func main() {
	stderr := log.New(os.Stderr, "", log.Lshortfile)

	if len(os.Args) != 2 {
		stderr.Fatalln("Must pass one argument.")
	}

	packs, err := doccover.ImportPackages(os.Args[1])
	if err != nil {
		stderr.Fatalf("unable to import: %s", err)
	}

	res := &Report{}
	funcs := doccover.ParsePackagesToFuncs(packs)
	publicFuncs, privateFuncs := doccover.UnzipFuncDecls(funcs)
	res.PublicFuncTotal = len(publicFuncs)
	res.PublicFuncCovered = doccover.CountFuncsWithDoc(publicFuncs)
	res.PrivateFuncTotal = len(privateFuncs)
	res.PrivateFuncCovered = doccover.CountFuncsWithDoc(privateFuncs)

	types := doccover.ParsePackagesToTypes(packs)
	publicTypes, privateTypes := doccover.UnzipTypeDecls(types)
	res.PublicTypeTotal = len(publicTypes)
	res.PublicTypeCovered = doccover.CountTypesWithDoc(publicTypes)
	res.PrivateTypeTotal = len(privateTypes)
	res.PrivateTypeCovered = doccover.CountTypesWithDoc(privateTypes)

	output, err := json.Marshal(res)
	if err != nil {
		stderr.Fatalf("unable to marshal json: %s", err)
	}

	fmt.Println(string(output))
}
